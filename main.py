import sys
from PyQt5 import uic, QtWidgets
from PyQt5.QtCore import pyqtSlot
from facebookads import PostAds
from facebook_business.exceptions import FacebookRequestError
from PyQt5.QtWidgets import QMessageBox, QTableWidget, QTableWidgetItem, QAction, QFileDialog
from blogspot import BlogSpot
import model
import create_blog

class Second(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Second, self).__init__(parent)


class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        self.link_text = ''
        self.post_text = ''
        self.template_file = ''
        self.amount_blog_value = 0
        self.data = {}
        # push to ads
        self.is_published = False
        self.call_action = ''
        self.attachments = []
        self.post_ads = None
        self.blogspot = BlogSpot()
        self.list_confg_google = []
        self.initui()
        self.show()
        self.model = model.Model()

    def initui(self):
        uic.loadUi('ui.ui', self)
        self.setWindowTitle('xXx')
        self.btn_submit.clicked.connect(self.submit)
        # self.btn_submit_post.clicked.connect(self.submit_spot)
        self.callToActionComboBox.addItem('LEARN_MORE')
        self.callToActionComboBox.addItem('LISTEN_NOW')
        self.callToActionComboBox.addItem('CONTACT_US')
        self.callToActionComboBox.activated.connect(self.handcompobox)
        # self.comboBox.activated.connect(self.select_conf_google)
        self.post_ads = PostAds()
        self.load_token_compo()

        self.combo_conf.activated.connect(self.select_config)
        self.compo_pages.activated.connect(self.select_page)
        self.compo_adaccount.activated.connect(self.select_adaccount)
        self.amount_blog.activated.connect(self.select_amount)

        self.cb_is_publish.stateChanged.connect(self.checkbox)
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.tableWidget.setColumnCount(4)
        self.tableWidget.setRowCount(10)
        self.tableWidget.setHorizontalHeaderLabels(["link", "name", "description", "thumb"])

        self.table_fb_token.setColumnCount(2)
        self.table_fb_token.setHorizontalHeaderLabels(["name", "token"])

        self.table_account.setColumnCount(2)
        self.table_account.setHorizontalHeaderLabels(["email", "password"])

        self.table_report.setColumnCount(2)
        self.table_report.setHorizontalHeaderLabels(["url", "title"])

        header = self.table_account.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        header = self.table_fb_token.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        header = self.table_report.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        self.btn_save_token.clicked.connect(self.save_fb_token)
        self.tbn_save_account.clicked.connect(self.save_accont)

        self.tabFb.currentChanged.connect(self.onChange)
        self.btn_delete_fb_token.clicked.connect(self.delete_fb_token)
        self.btn_delete_account.clicked.connect(self.delete_account)
        for x in range(5):
            self.amount_blog.addItem(str(x+1))

        self.create_blog_acc.clicked.connect(self.create_blog_byaccount)
        self.btn_template.clicked.connect(self.openFileNameDialog)

    def select_amount(self, index):
        value = self.amount_blog.itemText(index)
        self.amount_blog_value = int(value)

    def create_blog_byaccount(self):
        indexs = self.table_account.currentRow()
        # get item in row
        email = self.table_account.item(indexs, 1).text()
        password = self.table_account.item(indexs, 0).text()

        # create a post
        body = {
            "kind": "blogger#post",
            "title": self.title_post_sele.text(),
            "content": self.txt_post_sele.toPlainText()
        }
        post = body

        for x in range(self.amount_blog_value):
            create_blog.create_blog_by_account(self.template_file, email=password, password=email, post=post)

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            print(fileName)
            self.template_file = fileName
            self.label_template_file.setText(fileName)

    def select_page(self, index):
        name = self.compo_pages.itemText(index)
        print(name)
        for x in self.data['pages']:
            if name == x['name']:
                self.post_ads.page_token = x['access_token']
                self.post_ads.page_id = x['id']
                self.post_ads.set_conf()

    def select_adaccount(self, index):
        ad_account_id = self.compo_adaccount.itemText(index)
        self.post_ads.ad_account_id = ad_account_id
        self.post_ads.set_conf()

    def load_token_compo(self):
        fb = model.FbToken()
        list_token = fb.get_all()
        self.combo_conf.clear()
        for x in list_token:
            self.combo_conf.addItem(x.name)

    def get_value_table(self):
        for row in range(10):
            temp = {}
            if self.tableWidget.item(row, 0) \
                    and self.tableWidget.item(row, 1) \
                    and self.tableWidget.item(row, 2):
                temp['link'] = self.tableWidget.item(row, 0).text()
                temp['name'] = self.tableWidget.item(row, 1).text()
                temp['description'] = self.tableWidget.item(row, 2).text()
                # temp['image_hash'] = self.post_ads.get_images()
                videoid, urivideo = self.post_ads.upload_video()
                temp['video_id'] = videoid

                if self.tableWidget.item(row, 3) is None:
                    temp['picture'] = urivideo
                else:
                    temp['picture'] = self.tableWidget.item(row, 3).text()
                self.attachments.append(temp)

        print(self.attachments)

    # def select_conf_google(self, index):
    #     self.blogspot.file_scope = self.comboBox.itemText(index)
    #     self.blogspot.oauth()
    #     try:
    #         self.blogIdLineEdit.setText(self.blogspot.get_id_blog())
    #     except KeyError as e:
    #         QMessageBox.about(self, "message", "error")
    #     print(self.blogspot.file_scope)

    def handcompobox(self, index):
        self.call_action = self.callToActionComboBox.itemText(index)
        # get token with name
        print(self.call_action)

    def select_config(self, index):
        # get config by name
        name = self.combo_conf.itemText(index)
        # get token from name
        fb = model.FbToken()
        fb.name = name
        item = fb.get_token_with_name()
        # get page from token
        self.post_ads.access_token = item.token
        self.data['pages'] = self.post_ads.get_all_pages()
        self.data['adaccount'] = self.post_ads.get_adaccount_id()
        self.compo_pages.clear()
        for x in self.data['pages']:
            self.compo_pages.addItem(x['name'])
        for x in self.data['adaccount']:
            self.compo_adaccount.addItem(x['id'])

    def checkbox(self):
        if self.cb_is_publish.isChecked():
            self.is_published = True
        else:
            self.is_published = False

    # @pyqtSlot()
    # def submit_spot(self):
    #     body = {
    #         "kind": self.kindLineEdit.text(),
    #         "title": self.titleLineEdit.text(),
    #         "content": self.textEdit.toPlainText()
    #     }
    #
    #     self.blogspot.content = body
    #     self.blogspot.blog_id = self.blogIdLineEdit.text()
    #     self.blogspot.post()
    #     QMessageBox.about(self, "message", "successfully")

    @pyqtSlot()
    def submit(self):
        self.link_text = self.url.text()
        self.post_text = self.post.toPlainText()
        self.get_value_table()
        params = {
            'link': self.link_text,
            'message': self.post_text,
            'caption': '',
            'published': self.is_published,
            'multi_share_end_card': False,
            'multi_share_optimized': False,
            'child_attachments': self.attachments
        }

        print(params)

        self.post_ads.params = params
        try:
            self.post_ads.post_carousel()
            QMessageBox.about(self, "message", "successfully")
        except FacebookRequestError as e:
            print(e)
            QMessageBox.about(self, e.get_message(), e.api_error_message())

    def save_fb_token(self):
        txt_token = self.txt_fb_token.text()
        txt_name = self.txt_fb_name.text()
        fb_token = model.FbToken()
        fb_token.token = txt_token
        fb_token.name = txt_name
        kq = fb_token.save()
        if kq:
            QMessageBox.about(self, "message", "save successfully")
            self.load_data_table()
        else:
            QMessageBox.about(self, "message", kq)

    def save_accont(self):
        txt_email = self.txt_email.text()
        txt_password = self.txt_password.text()
        fb_token = model.BlogMn()
        fb_token.email = txt_email
        fb_token.password = txt_password
        kq = fb_token.save()
        if kq:
            QMessageBox.about(self, "message", "save successfully")
            self.load_data_table()
        else:
            QMessageBox.about(self, "message", kq)

        self.blogspot.file_scope = txt_email
        self.blogspot.oauth()
        self.reload_data_table_account()


    def reload_compo_blog(self):
        fb = model.BlogMn()
        reports = fb.get_all_report()
        self.table_report.setRowCount(len(reports))
        row = 0
        for x in reports:
            self.table_report.setItem(row, 0, QTableWidgetItem(x[0]))
            self.table_report.setItem(row, 1, QTableWidgetItem(x[1]))
            row += 1

    def reload_data_table_account(self):
        fb = model.BlogMn()
        row = 0
        listtoken = fb.get_all()
        self.table_account.setRowCount(len(listtoken))
        for x in listtoken:
            self.table_account.setItem(row, 0, QTableWidgetItem(x.password))
            self.table_account.setItem(row, 1, QTableWidgetItem(x.email))
            row += 1

    @pyqtSlot()
    def onChange(self):  # changed!
        print(self.tabFb.currentIndex())
        if self.tabFb.currentIndex() == 2:
            self.load_data_table()
        if self.tabFb.currentIndex() == 0:
            self.load_token_compo()
        if self.tabFb.currentIndex() == 3:
            self.reload_data_table_account()
        if self.tabFb.currentIndex() == 1:
            self.reload_compo_blog()

    def load_data_table(self):
        fb = model.FbToken()
        row = 0
        listtoken = fb.get_all()
        self.table_fb_token.setRowCount(len(listtoken))
        for x in listtoken:
            self.table_fb_token.setItem(row, 0, QTableWidgetItem(x.name))
            self.table_fb_token.setItem(row, 1, QTableWidgetItem(x.token))
            row += 1

    def delete_fb_token(self):
        indexs = self.table_fb_token.currentRow()
        # get item in row
        token = self.table_fb_token.item(indexs, 1).text()
        fb = model.FbToken()
        fb.token = token
        fb.delete_token()
        self.load_data_table()

    def delete_account(self):
        indexs = self.table_account.currentRow()
        # get item in row
        token = self.table_account.item(indexs, 0).text()
        fb = model.BlogMn()
        fb.email = token
        fb.delete_account()
        self.reload_data_table_account()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    sys.exit(app.exec_())
