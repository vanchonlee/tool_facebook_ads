from selenium import webdriver
import time
import random
import string
import os
import re
import blogspot

regex = r"blogID=([0-9]+)"


options = webdriver.ChromeOptions()
options.add_argument('--disable-extensions')
# options.add_argument('--headless')
options.add_argument('--disable-gpu')
options.add_argument('--no-sandbox')
options.add_argument('--incognito')
driver = webdriver.Chrome(chrome_options=options)


blog = blogspot.BlogSpot()


def authen(email, password):
    """
    xac thuc
    :param email:
    :param password:
    :return:
    """
    driver.get('https://www.blogger.com/about/?r=1-null_user')
    driver.implicitly_wait(5)
    driver.find_element_by_class_name("sign-in").click()
    driver.find_element_by_id("identifierId").send_keys(email)
    driver.find_element_by_id("identifierNext").click()
    driver.implicitly_wait(5)
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_id("passwordNext").click()
    time.sleep(5)


def create_blog():
    """
    tao blog moi
    :return:
    """
    driver.find_elements_by_class_name("blogg-menu-button-content")[0].click()
    driver.find_elements_by_class_name("menuButtonItem")[-1].click()

    title = ""
    for x in range(6):
        title+=random.choice(string.ascii_letters)
        title+=str(random.randint(1, 100))

    driver.find_element_by_id("newBlog-title").send_keys(title)
    driver.find_element_by_id("newBlog-address").send_keys(title+".blogspot.com")
    driver.find_element_by_id("newBlog-button").click()
    time.sleep(5)
    driver.find_element_by_id("newBlog-button").click()
    time.sleep(5)


def upload_template(path_theme):
    driver.find_element_by_id("blog-tab-template").click()
    time.sleep(5)
    # upload theme
    print("click upload theme")
    driver.find_element_by_xpath('//*[@id="blogger-app"]/div[3]/div[4]/div/div[1]/div[3]').click()
    print("click upload theme")
    time.sleep(5)
    driver.find_element_by_class_name("gwt-FileUpload").send_keys(path_theme)
    # click button upload
    driver.find_element_by_xpath('//*[@id="blogger-app"]/div[9]/div/div/div[2]/div/div/div[2]/div[1]/div/div/div[2]/div[3]/form/div/button').click()
    time.sleep(5)


def create_blog_by_account(theme, email, password, post):
    authen(email=email, password=password)
    driver.get('https://www.blogger.com/blogger.g#welcome')
    create_blog()
    upload_template(theme)

    # get blogid
    url = driver.current_url
    m = re.search(regex, url)
    blogId = m[1]

    blog.file_scope = email
    blog.oauth()
    blog.blog_id = blogId
    blog.content = post
    blog.post()


if __name__ == '__main__':
    a = os.path.join(os.path.dirname(__file__), "them.xml")
    print(a)
    create_blog_by_account(a, "vanchonlee1@gmail.com", "@huongly2511123")
