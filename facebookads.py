import sys
sys.path.append('/venv/lib/lib/python3.6/site-packages')
sys.path.append('/venv/lib/lib/python3.6/site-packages/facebook_business-3.0.0-py2.7.egg-info')
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.advideo import AdVideo
import json
import random
import facebook
import os

from facebook_business.adobjects.videothumbnail import VideoThumbnail

"""
access_token
ad_account_id
page_id
"""


class PostAds:
    def __init__(self):
        self.access_token = ''
        self.ad_account_id = ''
        self.page_token = ''
        self.face_confs = []
        self.page_id = ''
        self.params = {}
        self.account = None
        self.set_conf()

    def post_carousel(self):
        FacebookAdsApi.init(access_token=self.page_token)
        print(self.page_token)
        a = FacebookAdsApi.get_default_api(). \
            call('POST', (self.page_id, 'feed'), self.params)
        print(json.dumps(a, default=lambda o: o.__dict__))

    def get_images(self):
        images = self.account.get_ad_images()
        rand = random.randrange(0, len(images))
        print(images[rand].get_hash())
        return images[rand].get_hash()

    def get_videos(self):
        videos = self.account.get_ad_videos()
        rand = random.randrange(0, len(videos))
        return videos[rand].get_id()

    def set_conf(self):
        FacebookAdsApi.init(access_token=self.page_token)
        self.account = AdAccount(self.ad_account_id)

    def get_all_pages(self):
        graph = facebook.GraphAPI(access_token=self.access_token, version="3.1")
        a = graph.get_connections(id="me", connection_name="accounts")
        return a['data']

    def get_adaccount_id(self):
        graph = facebook.GraphAPI(access_token=self.access_token, version="3.1")
        a = graph.get_connections(id="me", connection_name="adaccounts")
        return a['data']

    def upload_video(self):
        filename = self.get_ran_file_in_folder('video')
        access_token = self.access_token
        FacebookAdsApi.init(access_token=access_token)
        # create video object
        video = AdVideo(parent_id="act_413449942102943")

        video_path = os.path.join(
            os.path.dirname(__file__),
            "video/"+filename
        )

        # set video fields
        video[AdVideo.Field.filepath] = video_path

        # remote create
        video.remote_create()
        video.waitUntilEncodingReady()

        return video.get_id(), video.get_thumbnails()[0]['uri']

    def get_ran_file_in_folder(self, dir):
        list_file = os.listdir(dir)
        ran = random.randint(0, len(list_file)-1)
        file = list_file[ran]
        print(file)
        return file


if __name__ == '__main__':
    a = PostAds()
    a.upload_video()
    # a.get_ran_file_in_folder('video')
