import os
from oauth2client import client
from oauth2client import tools
from oauth2client import file
from googleapiclient.http import build_http
from googleapiclient import discovery
# import parser
from model import BlogMn


class BlogSpot:
    def __init__(self):
        self.client_file = "client_secrets.json"
        self.client_id = ""
        self.client_secret = ""
        self.oauth_scope = "blogger"
        self.file_scope = "blogger"
        self.redirect_uri = ""
        self.flow = None
        self.service = None

        self.blog_id = ""
        self.content = ""

    def oauth(self):
        client_secrets = os.path.join(os.path.dirname(__file__), self.client_file)

        scope = 'https://www.googleapis.com/auth/' + self.oauth_scope

        self.flow = client.flow_from_clientsecrets(client_secrets,
                                                   scope=scope,
                                                   message=tools.message_if_missing(client_secrets))

        storage = file.Storage("acc/"+self.file_scope+".dat")
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            # self.flow.step1_get_authorize_url()
            credentials = tools.run_flow(self.flow, storage)

        http = credentials.authorize(http=build_http())
        self.service = discovery.build(self.oauth_scope, 'v3', http=http)

    def get_id_blog(self):
        blogs = self.service.blogs()
        thisusersblogs = blogs.listByUser(userId='self').execute()
        return thisusersblogs['items'][0]['id']

    def post(self):
        posts = self.service.posts()
        a = posts.insert(blogId=self.blog_id, body=self.content, isDraft=False).execute()
        bl = BlogMn()
        bl.add_link_report(url=a['url'], title=a['title'])


if __name__ == '__main__':
    blog = BlogSpot()
    blog.oauth()
    blog.get_id_blog()
