import os
import sqlite3


class Model:
    def __init__(self):
        self.pathdb = os.path.join(os.path.dirname(__file__), 'db/dbtools.db')
        self.conn = sqlite3.connect(self.pathdb)
        self.c = self.conn.cursor()
        self.check_table()

    def check_table(self):
        self.execute("create table if not exists fb_token (token text PRIMARY KEY, name text)")
        self.execute("create table if not exists account_blog (email text PRIMARY KEY, password text)")
        self.execute("create table if not exists report_blog (url text PRIMARY KEY, title text)")
        self.conn.commit()

    def execute(self, sql):
        try:
            self.c.execute(sql)
            self.conn.commit()
            return True
        except sqlite3.IntegrityError as msg:
            print(msg)
            pass
        except sqlite3.OperationalError as msg:
            return msg

    def execute2(self, sql):
        return self.c.execute(sql)


class FbToken:
    def __init__(self):
        self.token = ""
        self.name = ""
        self.table = "fb_token"
        self.m = Model()

    def save(self):
        sql = "INSERT INTO "+self.table+" VALUES ('%s','%s')" % (self.token, self.name)
        return self.m.execute(sql)

    def get_all(self):
        sql = "SELECT * FROM "+self.table
        list_return = []
        for x in self.m.execute2(sql):
            temp = FbToken()
            temp.name = x[1]
            temp.token = x[0]
            list_return.append(temp)
        return list_return

    def delete_token(self):
        sql = "DELETE FROM "+self.table+" WHERE `token` like '"+self.token+"'"
        print(self.m.execute(sql))
        print("delete "+self.token)

    def get_token_with_name(self):
        sql = "SELECT * FROM "+self.table+" WHERE `name` like '"+self.name+"'"
        for x in self.m.execute2(sql):
            temp = FbToken()
            temp.name = x[1]
            temp.token = x[0]
            return temp


class BlogMn:
    def __init__(self):
        self.email = ""
        self.password = ""
        self.table = "account_blog"
        self.m = Model()

    def save(self):
        sql = "INSERT INTO "+self.table+" VALUES ('%s','%s')" % (self.email, self.password)
        return self.m.execute(sql)

    def get_all(self):
        sql = "SELECT * FROM "+self.table
        list_return = []
        for x in self.m.execute2(sql):
            temp = BlogMn()
            temp.email = x[1]
            temp.password = x[0]
            list_return.append(temp)
        return list_return

    def delete_account(self):
        sql = "DELETE FROM "+self.table+" WHERE `email` like '"+self.email+"'"
        print(self.m.execute(sql))
        print("delete "+self.email)
        # delete file in acc
        path = os.path.join(os.path.dirname(__file__), self.email+".dat")
        if os.path.exists(path):
            os.remove(path)
            print("delete file is s")

    def get_account_with_email(self):
        sql = "SELECT * FROM "+self.table+" WHERE `email` like '"+self.email+"'"
        for x in self.m.execute2(sql):
            temp = BlogMn()
            temp.email = x[1]
            temp.password = x[0]
            return temp

    def add_link_report(self, url, title):
        sql = "INSERT INTO report_blog VALUES ('%s','%s')" % (url, title)
        return self.m.execute(sql)

    def get_all_report(self):
        sql = "SELECT * FROM report_blog"
        temp = []
        for x in self.m.execute2(sql):
            temp.append(x)
        return temp


if __name__ == '__main__':
    a = FbToken()
    for x in a.get_all():
        print(x.name)
